# Honey Dryer
(Well, to be honnest, this is just a basic temperature/moisture regulation board)

## Introduction


## Project description
Design a simple regulation mother board intended to be used with the "Raspberry Pi Zero W".

### Principles
The Raspberry daughter board can be installed on the "Honey dryer" board.

It supplies the daughter board with power (5VDC); connects the GPIO to temperature and/or moisture sensors; and offers actuators switching feature through DC relays (12VDC and 24VDC).

CAD software: Eagle

### Optimisations
None.
Components are choosen based on their direct availability in my personal stock.  Through-hole components are dirtly mixed with surface-mounted components with no care for the sake of good practice...  This project has no other ambition than beeing usable.


## Specifications
| Symbol | Parameter                                | Minimum | Typical | Maximum | Unit |
| ------ | ---------                                | ------- | ------- | ------- | ---- |
| Vi     | DC input voltage                         | 14      |         | 35      | V    |
| Vo1    | DC output voltage 1, embedded supply     |         | 5       |         | V    |
| Vo2    | DC output voltage 2, embedded supply     |         | 3.3     |         | V    |
| Vo3    | DC output voltage 3, actuators switching |         | 24      |         | V    |
| Vo4    | DC output voltage 4, actuators switching |         | 12      |         | V    |
